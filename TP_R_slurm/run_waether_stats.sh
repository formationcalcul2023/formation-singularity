#! /usr/bin/env bash

# PARAMETERS

#SBATCH -J waether                                          # JOB NAME
#SBATCH -A inrae-infra                                 # GROUP/ACCOUNT NAME
#SBATCH -p inrae                                        # PARTITION NAME
#SBATCH --output=/lustre/%u/logs/waether-%j.out             # OUTPUT FILE
#SBATCH --mem=1G                                       # MEMORY NEEDED FOR THE JOB
#SBATCH -c 1                                           # NUMBER OF CORES FOR THE JOB
#SBATCH --time=01:00:00                                # TIME LIMIT

# MY COMMAND HERE =>
module load singularity/3.6.3

singularity exec R.3.6.3_conda.sif Rscript rexample.R


