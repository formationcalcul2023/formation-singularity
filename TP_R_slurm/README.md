# Utilisation d'une image singularity sur le meso
## 
## 

## 1) Construction d'une recette Singularity 
* avec R-base (4.2.2)
* ggplot2 (>3.3) 
* en utilisant miniconda version 23.1.0 avec python 3.9
* basée sur une debian 10.13 ou ubuntu 20.04
## 2) générer l'image Singularity
## 3) Transérer l'image (.sif) sur meso HPC (avec le script R et le dataset)
## 4) Création du script de soumission slurm
## 5) soumettre le job
## 6) Transférer les resultats sur son PC

##

### de quel module j'ai besoin?
```bash
module avail
```





singularity/3.6.3

### how to run a sript:
```bash
singularity exec R.3.6.3_conda.sif Rscript rexample.R
```

