# first row contains variable names, comma is separator
# assign the variable id to row names
# on lit le fichier csv
weather = read.csv("WeatherDataset.csv", as.is=T)

colnames(weather)

# quelques stats de bases:
weather$Mean.TemperatureF
max(weather$MAXTEMP)
min(weather$MINTEMP)
mean(weather$AVGTEMP)
sd(weather$AVGTEMP)
length(weather$AVGTEMP)
mean(weather$AVGTEMP, na.rm=T)

# on fait quelques graphiques
plot(weather$AVGTEMP)
plot(weather$AVGTEMP, type="l")
hist(x = weather$AVGTEMP)
hist(x = weather$AVGTEMP, breaks=20, col="darkgreen", border="white")

