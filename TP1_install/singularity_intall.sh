#!/bin/bash
# Installation de singularity-ce en 3.9.2
set -e

#############  update & install debien packages #######
sudo apt-get update && sudo apt-get install -y \
    build-essential \
    uuid-dev \
    libgpgme-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    pkg-config \
    git \
    cryptsetup-bin

############# install GO ##############################
export VERSION=1.17.6 OS=linux ARCH=amd64 && \
wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz && \
sudo tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz && \
rm go$VERSION.$OS-$ARCH.tar.gz

echo '########### path for go & singularity ###############' >> ~/.bashrc
echo 'export PATH=/usr/local/go/bin:${PATH}' >> ~/.bashrc
export PATH=/usr/local/go/bin:${PATH}
export VERSION=3.9.2 && # adjust this as necessary \
    wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz && \
    tar -xzf singularity-ce-${VERSION}.tar.gz && \
    cd singularity-ce-${VERSION}

############# install singularity ######################
./mconfig && \
make -C ./builddir && \
sudo make -C ./builddir install
rm -fr singularity-ce-* 

############ for singularity code completion ##########
echo ". /usr/local/etc/bash_completion.d/singularity" >> $HOME/.bashrc

# reload the .bashrc after this script
echo "----------------------------------------"
echo "Please, execute the following command:"
echo "source $HOME/.bashrc"


