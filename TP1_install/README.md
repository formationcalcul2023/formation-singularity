# TP1, installation sur DEBIAN 11 de singularity 3.9.2
---

Sur un h�te Linux install� en DEBIAN 11, nous allons installer Singularity 3.9.2.

Pour cela, on va copier le fichier singularity_intall.sh contenant toutes les �tapes d'installation et l'�xecuter

```bash
ssh user@195.221.108.97
wget https://forgemia.inra.fr/formationcalcul2023/formation-singularity/-/raw/master/TP1_install/singularity_intall.sh
bash singularity_intall.sh
```
Voici le d�roul� du script:
* Installation des outils syst�mes n�cessaires �Singularity et le langage GO
* 1) mise � jour du syst�me et intallation de packages n�cessaires
* 2) Installation de GO sur l'h�te avec une version sp�cifique (ici 1.17.6)
* 3) Installation de  Singularity (compilation avec make) et ajout de la completion Singularity

